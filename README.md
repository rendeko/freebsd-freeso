# FreeSO on FreeBSD

This small guide details how I got FreeSO running on FreeBSD. Expect crashes, potential fires, doomsdays, anything that could go wrong.

### Prerequisites
We essentially just follow [dotequal's native Linux guide](http://forum.freeso.org/threads/freeso-on-linux-natively.6249), but install `libgdiplus` as well.
```
sudo pkg install mono
sudo pkg install sdl2
sudo pkg install cabextract
sudo pkg install libgdiplus
```

1. Download the latest [FreeSO Client](http://servo.freeso.org/viewLog.html?buildTypeId=ProjectDollhouse_TsoClient&buildId=lastSuccessful&buildBranch=%3Cdefault%3E&tab=artifacts&guest=1) and extract it wherever you'd like.

2. Download [macextras](http://freeso.org/stuff/macextras.zip) and overwrite the FreeSO client files with them.

3. Download an archive of The Sims Online (not linked here), as well as the latest [TSO-Version-Patcher](https://github.com/riperiperi/TSO-Version-Patcher/releases). Extract in different directories.

4. `cd` to where you extracted the TSO installer and run `cabextract -d /PathToYourFreeSOClient/game Data1.cab` (note that you will not have a game folder in your FreeSO client path, but you should add it to the end)

5. `cd` to where you extracted TSO-Version-Patcher and run `mono TSOVersionPatcherF.exe 1239toNI.tsop /PathToYourFreeSOClient/game`

6. Run `sudo chmod +x ./freeso.command` and `sudo chmod +x ./freeso3d.command` for executable permissions

### FreeBSD specific

1. Locate `MonoGame.Framework.dll.config` in your FreeSO client folder, and append the following lines
```
	<dllmap dll="soft_oal.dll" os="freebsd" cpu="x86" target="./x86/libopenal.so.1" />
	<dllmap dll="SDL2.dll" os="freebsd" cpu="x86-64" target="./x64/libSDL2-2.0.so.0" />
	<dllmap dll="soft_oal.dll" os="freebsd" cpu="x86-64" target="./x64/libopenal.so.1" />
```

2. Copy `/usr/local/lib/libSDL2-2.0.so.0.9.0` to `FreeSO/x86/libSDL2-2.0.so.0` and `FreeSO/x64/libSDL2-2.0.so.0`

3. Copy `/usr/local/lib/libopenal.so.1.19.1` to `FreeSO/x86/libopenal.so.1` and `FreeSO/x64/libopenal.so.1`

4. Finally, we can (hopefully) run the game with `bash ./freeso.client`

If you experience any issues, you may need to play around with which libraries you're overwriting.